﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    [HideInInspector]
    public Enemy enemy;

    public Transform healthCanvas;
    public Slider heathBar;
    public Transform targetPoint;

    private float health;

    void Awake ()
    {
        health = enemy.startHealth;
        heathBar.maxValue = health;
	}
	
	void Update ()
    {
        heathBar.value = health;

        Vector3 directionToCamera = Camera.main.transform.position - transform.position;
        directionToCamera.x = 0;

        Quaternion newRotation = Quaternion.LookRotation(-directionToCamera);
        healthCanvas.rotation = newRotation;
    }

    public void Damage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            GameManager.money += enemy.reward;
            GameObject.Destroy(gameObject);
        }
    }
}
