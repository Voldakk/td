﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class UITopBar : MonoBehaviour
{
    public Text wave;
    public Text money;

	void Update ()
    {
        wave.text = String.Format("Wave {0}", GameManager.waveSpawner.wave + 1);
        money.text = String.Format("{0}$", GameManager.money);
    }
}
