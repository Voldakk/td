﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TargetingNet
{
    public List<TargetSharing> towers;
    public List<Enemy> enemies;

    public int netId;
    public TargetingNet(int id)
    {
        netId = id;
        towers = new List<TargetSharing>();
        enemies = new List<Enemy>();
    }
}

public abstract class TargetSharing : Tower
{
    public TargetingNet net = null;

    public Enemy[] GetTargets()
    {
        return targetsInRange;
    }
    protected void SetupNet()
    {
        Tower[] hits = Physics.OverlapSphere(transform.position, range, LayerManager.towerMask).Select(c => c.GetComponent<Tower>()).ToArray();
        foreach (Tower tower in hits)
        {
            if (tower is TargetSharing && (tower as TargetSharing) != this)
            {
                TargetingNet otherNet = (tower as TargetSharing).net;
                if (net == null)
                {
                    otherNet.towers.Add(this);
                    net = otherNet;
                }
                else
                {
                    if (net.netId < otherNet.netId)
                    {
                        net.towers.AddRange(otherNet.towers);
                        foreach (TargetSharing otherTower in otherNet.towers)
                        {
                            otherTower.net = net;
                        }
                        GameManager.RemoveNet(otherNet);
                    }
                    else if (net.netId > otherNet.netId)
                    {
                        otherNet.towers.AddRange(net.towers);
                        foreach (TargetSharing otherTower in net.towers)
                        {
                            otherTower.net = otherNet;
                        }
                        GameManager.RemoveNet(net);
                    }
                }
            }
        }
        if (net == null)
        {
            net = GameManager.CreateNet();
            net.towers.Add(this);
        }
    }
    protected void ClearNet()
    {
        if(net != null)
        {
            net.towers.Remove(this);
        }
    }
    protected override void GetAllTargets()
    {
        targets = net.enemies.ToArray();
    }
    public override Tower Upgrade()
    {
        if (upgrade != null)
        {
            GameObject newTower = GameObject.Instantiate(upgrade);
            newTower.transform.position = transform.position;
            newTower.transform.rotation = transform.rotation;
            

            GameObject.Destroy(this.gameObject);
            return newTower.GetComponent<Tower>();
        }
        return null;
    }
}
