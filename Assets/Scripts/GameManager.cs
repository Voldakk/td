﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager self;

    public static int money;
    public int _money;
    public static int health;
    public int _health;

    [Range(1,2)]
    public int level;
    public static Level activeLevel;
    public List<Level> levels;

    public GameObject start;
    public GameObject end;
    public GameObject towerNode;
    public GameObject path;

    public Transform _mapHolder;
    public static Transform mapHolder;
    public Transform _towerHolder;
    public static Transform towerHolder;
    public Transform _enemyHolder;
    public static Transform enemyHolder;

    public static Camera mainCamera;
    public static WaveSpawner waveSpawner;

    public static GameObject[] enemies;
    public GameObject[] _enemies;

    public static GameObject[] towers;
    public GameObject[] _towers;

    public static bool placingTower = false;

    //Targeting nets
    public static List<TargetingNet> nets;

    void Awake()
    {
        self = this;
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        waveSpawner = GetComponent<WaveSpawner>();

        enemies = _enemies;
        towers = _towers;
        money = _money;
        health = _health;
        mapHolder = _mapHolder;
        towerHolder = _towerHolder;
        enemyHolder = _enemyHolder;


        nets = new List<TargetingNet>();
    }

    void Start()
    {
        levels = XMLInterface.Getlevels();
        LoadLevel(level);
    }

    void Update()
    {
        UpdateTargetingNets();
    }

    void LoadLevel(int id)
    {
        activeLevel = levels.Find(l => l.id == id);
        activeLevel.Load();
        mainCamera.transform.position = new Vector3((activeLevel.size.x - 1) / 2f, activeLevel.size.y, activeLevel.size.y);

        waveSpawner.LoadWaves(activeLevel);
    }

    void UpdateTargetingNets()
    {
        foreach (TargetingNet net in nets)
        {
            net.enemies.Clear();
            foreach (TargetSharing tower in net.towers)
            {
                if(tower.GetTargets() != null)
                {
                    net.enemies.AddRange(tower.GetTargets().Except(net.enemies));
                }
            }
        }
    }
    public static TargetingNet CreateNet()
    {
        int highNet = nets.Count == 0 ? 0 : nets.OrderBy(n => n.netId).Last().netId;
        TargetingNet newNet = new TargetingNet(highNet + 1);
        nets.Add(newNet);
        return newNet;
    }
    public static TargetingNet GetNet (int netId)
    {
        TargetingNet[] net = nets.Where(n => n.netId == netId).ToArray();
        if (net.Length > 0)
        {
            return net[0];
        }
        return null;
    }
    public static void RemoveNet(TargetingNet net)
    {
        nets.Remove(net);
    }
    public static void RemoveNet(int netId)
    {
        TargetingNet net = GetNet(netId);
        if (net != null)
            nets.Remove(net);
    }
}