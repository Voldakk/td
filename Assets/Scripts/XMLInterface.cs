﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

public class XMLInterface
{
    public static List<Level> Getlevels()
    {
        List<Level> levelList =
                (
                    from l in XDocument.Parse((Resources.Load("Levels") as TextAsset).text).Root.Elements("level")
                    select new Level
                    {
                        id = (int)l.Attribute("id"),
                        tiles =
                        (
                            l.Elements("row").Select(element => ((string)element.Value).ToCharArray().Select(c => c.ToString()).Reverse().ToArray())

                        ).ToArray()
                    }).ToList();
        
        List<Level> waves =
                (
                    from l in XDocument.Parse((Resources.Load("Waves") as TextAsset).text).Root.Elements("level")
                    select new Level
                    {
                        waves =
                        (
                            from w in l.Elements("wave")
                            select new Wave
                            {
                                waves =
                                (
                                    from e in w.Elements("enemy")
                                    select new SubWave
                                    {
                                        enemyId = (int)e.Attribute("id"),
                                        count = (int)e.Attribute("count")
                                    }
                                ).ToArray()
                            }
                        ).ToArray()
                    }
                ).ToList();

        for (int i = 0; i < levelList.Count; i++)
        {
            levelList[i].waves = waves[i].waves;
        }

        return levelList;
    }
}