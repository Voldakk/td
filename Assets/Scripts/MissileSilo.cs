﻿using UnityEngine;
using System.Collections;

public class MissileSilo : TargetSharing
{
    public Transform[] tubes;
    public GameObject missile;

    public float missileReloadTime;
    public float minMissileIntervel;

    Transform[] missileSpawnPoints;
    Animator[] tubeAnimators;
    private float[] missileReload;
    
    private int currentMissile;
    private float lastShootTime;

    void Start ()
    {
        base.SetupNet();
        missileSpawnPoints = new Transform[tubes.Length];
        tubeAnimators = new Animator[tubes.Length];
        missileReload = new float[tubes.Length];

        for (int i = 0; i < tubes.Length; i++)
        {
            missileSpawnPoints[i] = tubes[i].Find("Spawn");
            tubeAnimators[i] = tubes[i].GetComponent<Animator>();
            missileReload[i] = float.MinValue;
        }
    }
    void Update ()
    {
        base.UpdateTargets();

	    if(target != null)
        {
            if(Time.time >= missileReload[currentMissile] + missileReloadTime && Time.time >= lastShootTime + minMissileIntervel)
            {
                tubeAnimators[currentMissile].SetTrigger("Fire");

                GameObject newMissile = GameObject.Instantiate(missile);
                newMissile.transform.position = missileSpawnPoints[currentMissile].position;
                newMissile.GetComponent<Missile>().Set(target, this);

                lastShootTime = Time.time;
                missileReload[currentMissile] = Time.time;
                currentMissile++;
                if (currentMissile == tubes.Length)
                    currentMissile = 0;
            }
        }
	}
    public Transform GetNewTartget()
    {
        return target;
    }
    protected override void GetAllTargets()
    {
        targets = net.enemies.ToArray();
    }
}
