﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TrackingStation : TargetSharing
{
    public bool hasRadar;

    void Start()
    {
        base.SetupNet();
    }

    void Update()
    {
        if (hasRadar)
            base.UpdateTargets();
    }
}