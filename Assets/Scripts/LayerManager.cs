﻿using UnityEngine;
using System.Collections;

public class LayerManager : MonoBehaviour
{
    public static int towerLayer = 8;
    public static int towerMask = 1 << towerLayer;
    public static int enemyLayer = 9;
    public static int enemyMask = 1 << enemyLayer;
    public static int projectileLayer = 10;
    public static int towerNodeLayer = 11;
    public static int towerNodeMask = 1 << towerNodeLayer;
    public static int groundPlaneLayer = 12;
    public static int groundPlaneMask = 1 << groundPlaneLayer;
}
