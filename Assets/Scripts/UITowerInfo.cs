﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UITowerInfo : MonoBehaviour
{
    public Transform towerCamera;
    public Text towerName;
    public Text towerStats;
    public Text towerUpgradeText;
    public Button towerUpgradeButton;
    public Text towerUpgradeButtonText;

    private Tower selectedTower;
    private Tower tower;

    private Ray ray;
    private RaycastHit hit;

    void Update()
    {
        if (GameManager.placingTower)
        {
            return;
        }
        if (selectedTower != null)
        {
            UpdateTowerInfo();
        }

        ray = GameManager.mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 10000, LayerManager.towerMask, QueryTriggerInteraction.Ignore))
        {
            if (tower == null)
            {
                tower = hit.transform.GetComponentInParent<Tower>();
                tower.canvas.gameObject.SetActive(true);
            }
            else if (hit.transform != tower.transform)
            {
                if (selectedTower == null || hit.transform != selectedTower.transform)
                {
                    tower.canvas.gameObject.SetActive(false);
                    tower = hit.transform.GetComponentInParent<Tower>();
                    tower.canvas.gameObject.SetActive(true);
                }
            }
        }
        else if (tower != null)
        {
            tower.canvas.gameObject.SetActive(false);
            tower = null;
        }
        if (Input.GetMouseButtonDown(0) && tower != null)
        {
            selectedTower = tower;
            selectedTower.canvas.gameObject.SetActive(true);
            towerCamera.gameObject.SetActive(true);
            towerCamera.position = tower.transform.position + new Vector3(0, 1.9f, 1.4f);
        }
    }
    void UpdateTowerInfo()
    {
        towerName.text = selectedTower.displayName;

        float damage = 0;
        if (selectedTower is Turret)
            damage = (selectedTower as Turret).damage;
        else if (selectedTower is MissileSilo)
            damage = (selectedTower as MissileSilo).missile.GetComponent<Missile>().damage;
        else if (selectedTower is ShockTower)
            damage = (selectedTower as ShockTower).dps;

        towerStats.text = string.Format(
@"Cost: {0}
Damage: {1}
Range:  {2}
", selectedTower.cost, damage, selectedTower.range);

        if (selectedTower is TargetSharing && (selectedTower as TargetSharing).net != null)
            towerStats.text += string.Format("Net: {0}", (selectedTower as TargetSharing).net.netId);

        if (selectedTower.upgrade != null)
        {
            towerUpgradeText.text = string.Format("Next level: \n{0}", selectedTower.upgrade.GetComponent<Tower>().displayName);
            towerUpgradeButton.gameObject.SetActive(true);
            towerUpgradeButtonText.text = string.Format("Upgrade ${0}", selectedTower.upgrade.GetComponent<Tower>().cost);
        }
        else
        {
            towerUpgradeText.text = "";
            towerUpgradeButton.gameObject.SetActive(false);
        }
    }
    public void TowerUpgradeButtonClick()
    {
        if(selectedTower != null && selectedTower.upgrade != null && GameManager.money >= selectedTower.upgrade.GetComponent<Tower>().cost)
        {
            selectedTower = selectedTower.Upgrade();
            selectedTower.canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, selectedTower.range * 2);
            selectedTower.canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, selectedTower.range * 2);

            GameManager.money -= selectedTower.cost;
        }
    }
}
