﻿using UnityEngine;
using System.Collections;


public class CameraControls : MonoBehaviour
{
    public enum CamType { Per, Ortor};

    public float zoonSpeed;
    public float panSpeed;
    public CamType cameraType;

    private new Camera camera;

    void Start ()
    {
        camera = GetComponent<Camera>();
    }
	void Update ()
    {
        if (cameraType == CamType.Per)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                transform.Translate(Vector3.back * panSpeed * Time.deltaTime, Space.World);
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                transform.Translate(Vector3.forward * panSpeed * Time.deltaTime, Space.World);
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                transform.Translate(Vector3.right * panSpeed * Time.deltaTime, Space.World);
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                transform.Translate(Vector3.left * panSpeed * Time.deltaTime, Space.World);

            transform.Translate(Vector3.down * Input.mouseScrollDelta.y * zoonSpeed * Time.deltaTime, Space.World);
        }
        else if( cameraType == CamType.Ortor)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                transform.Translate((Vector3.back + Vector3.right) * panSpeed * Time.deltaTime * camera.orthographicSize /10, Space.World);
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                transform.Translate((Vector3.forward + Vector3.left) * panSpeed * Time.deltaTime * camera.orthographicSize/10, Space.World);
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                transform.Translate((Vector3.right + Vector3.forward) * panSpeed * Time.deltaTime * camera.orthographicSize/10, Space.World);
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                transform.Translate((Vector3.left + Vector3.back) * panSpeed * Time.deltaTime * camera.orthographicSize/10, Space.World);

            camera.orthographicSize -= Input.mouseScrollDelta.y * zoonSpeed * Time.deltaTime * camera.orthographicSize / 10;
            if (camera.orthographicSize < 0.3f)
                camera.orthographicSize = 0.3f;
        }
    }
}
