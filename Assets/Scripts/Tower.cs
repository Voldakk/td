﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum Targeting { closest, first, closestTilRange, none }

public class Tower : MonoBehaviour
{
    public Transform canvas;
    public string displayName;
    public int cost;
    public float range;
    public Targeting targeting;

    public GameObject upgrade;

    protected Transform target;
    protected Enemy[] targetsInRange;
    protected Enemy[] targets;

    protected virtual void GetAllTargets()
    {
        //Finner alle fiender innenfor rekkevidde
        targets = targetsInRange;
    }
    public void UpdateTargets()
    {
        //Om vi ikke ser etter noen mål
        if(targeting == Targeting.none)
        {
            //Nullstiller stuff
            targetsInRange = null;
            targets = null;
            target = null;
            return;
        }
        //Finner alle fiender innenfor rekkevidde
        targetsInRange = Physics.OverlapSphere(transform.position, range, LayerManager.enemyMask).Select(c => c.GetComponent<Enemy>()).ToArray();

        //Oppdaterer mulig mål
        GetAllTargets();

        //Avslutter om det ikke er noen
        if (targets.Length == 0)
            return;

        if (targeting == Targeting.closest)
        {
            //Distansen til fienden
            float dist;

            //Distansen til den nærmeste fienden
            float minDist = Mathf.Infinity;
            Transform newTarget;

            //For hver collider
            foreach (Enemy enemy in targets)
            {
                if (enemy == null)
                    continue;

                newTarget = enemy.health.targetPoint;
                //Finner distansen
                dist = (newTarget.position - this.transform.position).magnitude;

                //Om det er den nærmeste hittil
                if (dist < minDist)
                {
                    //Husker på det
                    minDist = dist;
                    target = newTarget;
                }
            }
        }
        else if (targeting == Targeting.closestTilRange)
        {
            if (target == null || !targets.ToList().Select(e => e.health.targetPoint).Contains(target))
            {
                //Distansen til fienden
                float dist;
                //Distansen til den nærmeste fienden
                float minDist = Mathf.Infinity;
                Transform newTarget;
                //For hver collider
                foreach (Enemy enemy in targets)
                {
                    newTarget = enemy.health.targetPoint;
                    //Finner distansen
                    dist = (newTarget.position - this.transform.position).magnitude;

                    //Om det er den nærmeste hittil
                    if (dist < minDist)
                    {
                        //Husker på det
                        minDist = dist;
                        target = newTarget;
                    }
                }
            }
        }
        else if (targeting == Targeting.first)
        {
            target = targets.OrderBy(e => e.movement.waypointIndex).Last().health.targetPoint;
        }
    }
    public virtual Tower Upgrade()
    {
        if(upgrade != null)
        {
            GameObject newTower = GameObject.Instantiate(upgrade);
            newTower.transform.position = transform.position;
            newTower.transform.rotation = transform.rotation;

            GameObject.Destroy(this.gameObject);
            return newTower.GetComponent<Tower>();
        }
        return null;
    }
}