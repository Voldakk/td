﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float angularVelocity;

    private Effect effect;
    private Transform target;
    private float damage;

    new Rigidbody rigidbody;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        gameObject.layer = LayerManager.projectileLayer;
    }
    public void Set(Transform target, float damage, Effect effect)
    {
        this.target = target;
        this.damage = damage;
        this.effect = effect;
    }
    void Update()
    {
        //SMålig varmesøkende?
        if (target != null)
        {
            //Retningen mot målet
            Vector3 targetDirection = target.position - transform.position;

            //Rotasjonen som peker mot målet
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

            //Roterer mot den riktige veien 
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, angularVelocity * Time.deltaTime);

            //Setter farten mot veien prosjektilet peker
            rigidbody.velocity = transform.rotation * Vector3.forward * rigidbody.velocity.magnitude;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //Grab the health component
        EnemyHealth health = other.GetComponent<EnemyHealth>();

        //If we hit something we can damage
        if(health != null)
        {
            //Apply the damage
            health.Damage(damage);

            //Apply the effect if we have one
            if(effect != null)
                health.enemy.ApplyEffect(effect);
        }

        //Ignore towers
        if (other.gameObject.layer != LayerManager.towerLayer)
            GameObject.Destroy(gameObject);
    }
}
