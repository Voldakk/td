﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    [HideInInspector]
    public Enemy enemy;

    private Vector3 waypoint;
    public int waypointIndex = 0;

	void Start ()
    {
        waypoint = GameManager.activeLevel.waypoints[waypointIndex];
	}

	void Update ()
    {
        transform.Translate((waypoint - transform.position).normalized * enemy.movementSpeed * Time.deltaTime);
	    if(Vector3.Distance(transform.position, waypoint) < 0.1f)
        {
            waypointIndex++;
            if (waypointIndex == GameManager.activeLevel.waypoints.Count)
                End();
            else
                waypoint = GameManager.activeLevel.waypoints[waypointIndex];
        }
	}
    void End()
    {
        GameManager.health -= enemy.damage;
        GameObject.Destroy(gameObject);
    }
}
