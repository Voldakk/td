﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CameraSize : MonoBehaviour
{
    private float width;
    private float height;
    private float aspect;
    private float y;
    new private Camera camera;

    void Start ()
    {
        camera = GetComponent<Camera>();
        width = camera.rect.width;
    }

	void Update ()
    {
        
        aspect = (float)Screen.width / (float)Screen.height;
        height = width * aspect;

        y = 1 - ((30f / Screen.width) * aspect) - height;

        camera.rect = new Rect(camera.rect.x, y, width, height);
	}
}
