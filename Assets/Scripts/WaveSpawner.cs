﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wave
{
    public SubWave[] waves;
}
public class SubWave
{
    public int enemyId;
    public int count;
}

public class WaveSpawner : MonoBehaviour
{
    public float spawnInterval = 1;

    private Wave[] waves;
    private bool spawning = false;

    private float lastSpawnTime = 0;
    public int wave = 0;
    private int sub = 0;
    private int count = 0;

	void Update ()
    {
	    if(spawning)
        {
            if (wave >= waves.Length)
            {
                spawning = false;
            }
            else
            {
                if (sub >= waves[wave].waves.Length)
                {
                    wave++;
                    sub = 0;
                }
                else
                {
                    if (count == waves[wave].waves[sub].count)
                    {
                        sub++;
                        count = 0;
                    }
                    else if(Time.time > lastSpawnTime + spawnInterval)
                    {
                        lastSpawnTime = Time.time;
                        count++;
                        Spawn(waves[wave].waves[sub].enemyId);
                    }
                }
            }
        }
	}
    void Spawn(int id)
    {
        GameObject newEnamy = GameManager.Instantiate(GameManager.enemies[id]);
        newEnamy.transform.position = GameManager.activeLevel.waypoints[0];
        newEnamy.transform.parent = GameManager.enemyHolder;
    }

    public void LoadWaves(Level level)
    {
        waves = level.waves;
        spawning = true;
    }
    public void NextWave()
    {
        wave++;
        spawning = true;
    }
}
