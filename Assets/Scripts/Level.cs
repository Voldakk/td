﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level
{
    public int id;
    public Vector2 size;
    public string[][] tiles;
    public List<Vector3> waypoints;
    public Wave[] waves;
    public void Load()
    {
        size = new Vector2(tiles[0].Length, tiles.Length);

        //Just have to set it to something
        GameObject newObject = new GameObject();
        GameObject.Destroy(newObject);

        //Load objects
        for (int z = 0; z < tiles.Length; z++)
        {
            for (int x = 0; x < tiles[z].Length; x++)
            {
                switch (tiles[z][x])
                {
                    case "S":
                        newObject = GameObject.Instantiate(GameManager.self.start);
                        break;
                    case "E":
                        newObject = GameObject.Instantiate(GameManager.self.end);
                        break;
                    case "#":
                        newObject = GameObject.Instantiate(GameManager.self.towerNode);
                        break;
                    case " ":
                        newObject = GameObject.Instantiate(GameManager.self.path);
                        break;
                }
                newObject.isStatic = true;
                newObject.transform.position = GetPos(z, x);
                newObject.transform.parent = GameManager.mapHolder;
            }
        }

        //Waypoints
        waypoints = new List<Vector3>();

        //Finner startnoden
        Vector2 tile = Vector2.down;
        for (int z = 0; z < tiles.Length; z++)
        {
            for (int x = 0; x < tiles[z].Length; x++)
            {
                if (tiles[z][x] == "S")
                {
                    tile = new Vector2(z,x);
                    break;
                }

            }
            if (tile != Vector2.down)
                break;
        }

        waypoints.Add(GetPos(tile));

        string current = "S";
        Vector2 prev = Vector2.down;

        while(current != "E")
        {
            for (int z = -1; z <= 1; z ++)
            {
                for (int x = -1; x <= 1; x ++)
                {
                    if (
                        (z != 0 && x != 0) || //Flytter ikke diagonalt
                        (z == 0 && x == 0) ||
                        tile.x + z < 0 ||
                        tile.x + z >= size.y ||
                        tile.y + x < 0 ||
                        tile.y + x >= size.x
                        )
                    {
                        continue;
                    }
                    if (tiles[(int)tile.x+z][(int)tile.y + x] == " " || tiles[(int)tile.x + z][(int)tile.y + x] == "E")
                    {
                        if (tile + new Vector2(z, x) == prev)
                            continue;
                        prev = tile;
                        tile += new Vector2(z, x);
                        current = GetTile(tile);
                        waypoints.Add(GetPos(tile));
                        continue;
                    }
                }
            }
        }
    }
    private string GetTile(Vector3 pos)
    {
        return tiles[(int)pos.z][(int)pos.x];
    }
    private string GetTile(Vector2 pos)
    {
        //                  Z           X
        return tiles[(int)pos.x][(int)pos.y];
    }
    private string GetTile(Vector2 current, Vector2 dir)
    {
        current += dir;
        return GetTile(current);
    }
    private Vector3 GetPos(Vector2 tile)
    {
        //                   X     Y    Z
        return new Vector3(tile.y, 0, tile.x);
    }
    private Vector3 GetPos(int z, int x)
    {
        return new Vector3(x, 0, z);
    }
}