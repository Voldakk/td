﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Missile : MonoBehaviour
{
    public GameObject explotion;
    public float height;
    public float damage;
    public float blastRadius;
    public float speed;
    public float angularVelocity;

    private MissileSilo silo;
    private Transform target;
    new private Rigidbody rigidbody;

    private float fireTime;

    void Start()
    {
        fireTime = Time.time;

        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = Vector3.up * speed;
        transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.up);
    }

    void Update ()
    {
        if(target == null)
        {
            target = silo.GetNewTartget();
        }
        if (height != 0)
        {
            if (transform.position.y >= height)
                height = 0;
        }
        else if (target != null)
        {
            //Retningen mot målet
            Vector3 targetDirection = target.position - transform.position;

            //Rotasjonen som peker mot målet
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

            //Roterer mot den riktige rotasjonen 
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, angularVelocity * Time.deltaTime);

            //Setter farten mot retningen prosjektilet peker
            rigidbody.velocity = transform.rotation * Vector3.forward * rigidbody.velocity.magnitude;
        }
    }

    public void Set(Transform newTarget, MissileSilo newSilo)
    {
        target = newTarget;
        silo = newSilo;
    }

    void OnTriggerEnter(Collider other)
    {
        if (Time.time > fireTime + 1f)
        {
            EnemyHealth[] targetsInRange = Physics.OverlapSphere(transform.position, blastRadius, LayerManager.enemyMask).Select(c => c.GetComponent<EnemyHealth>()).ToArray();

            for (int i = 0; i < targetsInRange.Length; i++)
            {
                targetsInRange[i].Damage(damage);
            }
            GameObject ex = GameObject.Instantiate(explotion);
            ex.transform.position = transform.position;
            ex.transform.localScale = Vector3.one * blastRadius/10;
            GameObject.Destroy(gameObject);
        }
    }
}
