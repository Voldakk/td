﻿using UnityEngine;
using System.Collections;

public class ShockTower : Tower
{
    public float dps;
    public Transform max;
    public Transform min;
    private LineRenderer lineRenderer;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

	void Update ()
    {
        base.UpdateTargets();

	    if(targets != null && targets.Length > 0)
        {
            lineRenderer.SetVertexCount(targets.Length * 3);
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].health.Damage(dps * Time.deltaTime);
                Vector3 rayOrigin = transform.position + Vector3.up * Random.Range(min.position.y, max.position.y);
                Vector3 rayTarget = targets[i].health.targetPoint.position + Vector3.up * Random.Range(-0.2f, 0.2f);

                lineRenderer.SetPosition(i*3, rayOrigin);
                lineRenderer.SetPosition(i*3 + 1, rayTarget);
                lineRenderer.SetPosition(i*3 + 2, rayOrigin);
            }
        }
        else
        {
            lineRenderer.SetVertexCount(0);
        }
	}
}
