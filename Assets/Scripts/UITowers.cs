﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UITowers : MonoBehaviour
{
    private Tower selectedTower;
    private int selectedTowerId;
    private TowerNode towerNode;
    private Ray ray;
    private RaycastHit hit;

    public List<Button> towerButtons;

	void Update ()
    {
        //Buttons
        for (int i = 0; i < towerButtons.Count; i++)
        {
            towerButtons[i].interactable = GameManager.money >= GameManager.towers[i].GetComponent<Tower>().cost;
        }
        //Selection
        if (selectedTower != null)
        {
            ray = GameManager.mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000, LayerManager.towerNodeMask, QueryTriggerInteraction.Ignore))
            {
                towerNode = hit.transform.GetComponent<TowerNode>();
                selectedTower.transform.position = hit.transform.position + Vector3.up * 0.1f;
            }
            else
            {
                towerNode = null;
                ray = GameManager.mainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 10000, LayerManager.groundPlaneMask, QueryTriggerInteraction.Ignore))
                {
                    selectedTower.transform.position = hit.point + Vector3.up * 0.2f;
                }
            }
            if(Input.GetMouseButtonDown(0) && towerNode != null && towerNode.Empty() && GameManager.money >= selectedTower.cost)
            {
                GameManager.money -= selectedTower.cost;
                towerNode.SetTower(selectedTowerId);
                selectedTower.enabled = true;
                selectedTower.canvas.gameObject.SetActive(false);
                
                if(Input.GetKey(KeyCode.LeftControl) && GameManager.money >= selectedTower.cost)
                {
                    SpawnTower();
                }
                else
                {
                    selectedTower = null;
                    GameManager.placingTower = false;
                }
            }
            if (Input.GetMouseButtonDown(1))
            {
                Destroy(selectedTower.gameObject);
                GameManager.placingTower = false;
            }
        }
	}

    public void ButtonClicked(Button button)
    {

        int id = towerButtons.IndexOf(button);
        if(selectedTower != null)
        {
            GameObject.Destroy(selectedTower.gameObject);
        }
        selectedTowerId = id;
        SpawnTower();

        GameManager.placingTower = true;
    }
    void SpawnTower()
    {
        selectedTower = GameObject.Instantiate(GameManager.towers[selectedTowerId]).GetComponent<Tower>();
        selectedTower.enabled = false;
        selectedTower.canvas.gameObject.SetActive(true);
        selectedTower.canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, selectedTower.range * 2);
        selectedTower.canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, selectedTower.range * 2);
        selectedTower.transform.parent = GameManager.towerHolder;
    }
}
