﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Turret : Tower
{
    public Transform turret;
    public Transform[] barrels;
    private Transform[] barrelEnds;
    public GameObject projectile;

    public float damage;
    public float firerate;
    public float projectileSpeed;
    public float turretRotationSpeed;
    public float barrelElevationSpeed;

    public Effect effect;

    private float lastShootTime = 0f;
    private int currentBarrel = 0;

    void Start()
    {
        barrelEnds = new Transform[barrels.Length];
        for (int i = 0; i < barrels.Length; i++)
        {
            barrelEnds[i] = barrels[i].Find("BarrelEnd");
        }
    }
    void Update()
    {
        base.UpdateTargets();

        if (target != null)
        {
            //Retningen mot målet
            Vector3 targetDirection = target.transform.position - barrelEnds[currentBarrel].position;
            //Retningen mot målet om man ignorerer høyde
            Vector3 flatDirection = targetDirection - Vector3.up * targetDirection.y;

            //Roterer mounten
            Quaternion mountRotation = Quaternion.LookRotation(flatDirection, Vector3.up);
            turret.rotation = Quaternion.RotateTowards(turret.rotation, mountRotation, turretRotationSpeed * Time.deltaTime);

            //Roterer våpenet
            float angle = Mathf.Atan2(targetDirection.y, new Vector2(flatDirection.x, flatDirection.z).magnitude) * Mathf.Rad2Deg;
            barrels[currentBarrel].transform.localRotation = Quaternion.RotateTowards(barrels[currentBarrel].transform.localRotation, Quaternion.Euler(new Vector3(-angle, 0, 0)), barrelElevationSpeed * Time.deltaTime);

            //Om mounten er riktig rotert
            if (Quaternion.Angle(turret.rotation, mountRotation) < 0.1f)
            {
                //Om våpenet er riktig rotert
                if (Quaternion.Angle(barrels[currentBarrel].transform.localRotation, Quaternion.Euler(new Vector3(-angle, 0, 0))) < 0.1f)
                {
                    //Oppdaterer retningen til fra tuppen av våpenet
                    targetDirection = target.transform.position - barrelEnds[currentBarrel].position;
                    if (Time.time >= lastShootTime + (60f / firerate))
                    {
                        //Husker når vi sist skjøt
                        lastShootTime = Time.time;

                        //Lager et nytt prosjektil
                        GameObject newProjectile = Instantiate<GameObject>(projectile);

                        //Setter layeret til Default
                        newProjectile.layer = 0;
                        //Posisjonerer det
                        newProjectile.transform.position = barrelEnds[currentBarrel].position;
                        newProjectile.transform.rotation = barrelEnds[currentBarrel].rotation;

                        //Sier til prosjektilet hva set skal gjøre
                        newProjectile.GetComponent<Projectile>().Set(target, damage, effect);

                        //Setter farten
                        newProjectile.GetComponent<Rigidbody>().velocity = ((target.transform.position - barrelEnds[currentBarrel].position).normalized) * projectileSpeed;

                        //Bytterløp
                        currentBarrel++;
                        if (currentBarrel == barrels.Length)
                            currentBarrel = 0;
                    }
                }
            }
        }
    }
}
