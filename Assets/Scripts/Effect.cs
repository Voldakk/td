﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Effect
{
    public float dps;
    public float slow;
    public float duration;
    [HideInInspector]
    public float timeApplied;
}