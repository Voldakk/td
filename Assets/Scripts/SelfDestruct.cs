﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour
{
    public float lifetime;
    void Start()
    {
        StartCoroutine(Destroy());
    }
    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(lifetime);
        GameObject.Destroy(gameObject);
    }
}