﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    public float startHealth;
    public float baseMovementSpeed;
    [HideInInspector]
    public float movementSpeed;

    public int damage;
    public int reward;

    [HideInInspector]
    public EnemyHealth health;
    [HideInInspector]
    public EnemyMovement movement;

    private List<Effect> effects;

	void Awake ()
    {
        health = GetComponent<EnemyHealth>();
        movement = GetComponent<EnemyMovement>();

        health.enemy = this;
        movement.enemy = this;

        effects = new List<Effect>();

        movementSpeed = baseMovementSpeed;
	}

    void Update()
    {
        //Reset the movement speed
        movementSpeed = baseMovementSpeed;

        //List off effects to remove
        List<Effect> remove = new List<Effect>();

        //For each effect
        foreach (Effect effect in effects)
        {
            //If the duration is out
            if (Time.time > effect.timeApplied + effect.duration)
            {
                //Flag the effect to be removed
                remove.Add(effect);
                continue;
            }
            //Apply damage
            health.Damage(effect.dps * Time.deltaTime);

            //Apply slow
            movementSpeed *= (100 - effect.slow) / 100;
        }
        //Remove all flagged effects
        foreach (Effect effect in remove)
        {
            effects.Remove(effect);
        }
    }

    public void ApplyEffect(Effect effect)
    {
        //Add the effect to the list
        effects.Add(effect);

        //Remember when it was applied
        effect.timeApplied = Time.time;
    }
}
