﻿using UnityEngine;
using System.Collections;

public class TowerNode : MonoBehaviour
{
    private int tower = -1;

    public bool Empty()
    {
        return tower < 0;
    }
    public void SetTower(int id)
    {
        tower = id;
    }
}
